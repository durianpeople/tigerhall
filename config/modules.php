<?php

return [

	'default_module' => null,
	'modules' => [
		'tiger_tracker' => [
			'module_class' => '\App\Modules\TigerTracker\Module',
			'enabled' => true,
		],
	],
];