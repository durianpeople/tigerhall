<?php

return [
    'driver' => 'mysql',
    'host' => env('TIGER_TRACKER_DB_HOST', 'localhost'),
    'port' => env('TIGER_TRACKER_DB_PORT', '1433'),
    'database' => env('TIGER_TRACKER_DB_DATABASE', 'forge'),
    'username' => env('TIGER_TRACKER_DB_USERNAME', 'forge'),
    'password' => env('TIGER_TRACKER_DB_PASSWORD', ''),
    'charset' => 'utf8',
    'prefix' => '',
    'prefix_indexes' => true,
];