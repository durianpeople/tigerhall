CREATE DATABASE IF NOT EXISTS `system_tigerhall`;
CREATE DATABASE IF NOT EXISTS `tigerhall`;

CREATE USER 'root'@'localhost' IDENTIFIED BY 'root';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';