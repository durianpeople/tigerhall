{
  "openapi": "3.0.3",
  "info": {
    "title": "Tigerhall Kittens",
    "version": "1.0.0"
  },
  "servers": [
    {
      "url": "http://127.0.0.1:8000"
    }
  ],
  "paths": {
    "/api/tiger_tracker/tigers": {
      "post": {
        "summary": "Create a new tiger",
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "type": "object",
                "properties": {
                  "name": {
                    "type": "string",
                    "description": "Name of the tiger",
                    "nullable": false,
                    "example": "A Tiger Name"
                  },
                  "birth_date": {
                    "type": "integer",
                    "format": "unix-timestamp",
                    "description": "Tiger's date of birth, specified by UNIX timestamp",
                    "example": "1648904034"
                  },
                  "last_seen_timestamp": {
                    "type": "integer",
                    "format": "unix-timestamp",
                    "description": "When the tiger is last seen, specified in UNIX timestamp",
                    "example": "1648904034"
                  },
                  "last_seen_latitude": {
                    "type": "number",
                    "format": "float",
                    "description": "Coordinate (latitude) of the tiger's last seen",
                    "example": "1.385286074287265"
                  },
                  "last_seen_longitude": {
                    "type": "number",
                    "format": "float",
                    "description": "Coordinate (longitude) of the tiger's last seen",
                    "example": "103.79614166886488"
                  }
                }
              }
            }
          }
        },
        "responses": {
          "201": {
            "description": "Returned when a tiger is successfully created",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "message": {
                      "type": "string",
                      "description": "Success message",
                      "example": "Tiger successfully created"
                    }
                  }
                }
              }
            }
          },
          "400": {
            "$ref": "#/components/responses/400"
          },
          "500": {
            "$ref": "#/components/responses/500"
          }
        }
      },
      "get": {
        "summary": "List all tigers",
        "parameters": [
          {
            "name": "last_seen_start_range",
            "in": "query",
            "description": "Specify the start range of the tigers' last seen (inclusive)",
            "schema": {
              "type": "integer",
              "format": "unix-timestamp"
            }
          },
          {
            "name": "last_seen_end_range",
            "in": "query",
            "description": "Specify the end range of the tigers' last seen (exclusive)",
            "schema": {
              "type": "integer",
              "format": "unix-timestamp"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Returned when list can be obtained",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "data": {
                      "type": "array",
                      "items": {
                        "type": "object",
                        "properties": {
                          "id": {
                            "type": "string",
                            "format": "uuid",
                            "description": "ID of the tiger",
                            "example": "3d6ad209-d21b-4bde-aedb-e8f93293514b"
                          },
                          "name": {
                            "type": "string",
                            "description": "Name of the tiger"
                          },
                          "birth_date": {
                            "type": "integer",
                            "format": "unix-timestamp",
                            "description": "Tiger's date of birth, specified by UNIX timestamp",
                            "example": "1648904034"
                          },
                          "last_seen_timestamp": {
                            "type": "integer",
                            "format": "unix-timestamp",
                            "description": "When the tiger is last seen, specified in UNIX timestamp",
                            "example": "1648904034"
                          },
                          "last_seen_latitude": {
                            "type": "number",
                            "format": "float",
                            "description": "Coordinate (latitude) of the tiger's last seen",
                            "example": "1.385286074287265"
                          },
                          "last_seen_longitude": {
                            "type": "number",
                            "format": "float",
                            "description": "Coordinate (longitude) of the tiger's last seen",
                            "example": "103.79614166886488"
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          },
          "500": {
            "$ref": "#/components/responses/500"
          }
        }
      }
    },
    "/api/tiger_tracker/tiger/sightings": {
      "post": {
        "summary": "Create a new sighting of a tiger",
        "requestBody": {
          "content": {
            "multipart/form-data": {
              "schema": {
                "type": "object",
                "properties": {
                  "latitude": {
                    "type": "number",
                    "format": "float",
                    "description": "Latitude of the sighting",
                    "example": "1.385286074287265"
                  },
                  "longitude": {
                    "type": "number",
                    "format": "float",
                    "description": "Longitude of the sighting",
                    "example": "103.79614166886488"
                  },
                  "timestamp": {
                    "type": "integer",
                    "format": "unix-timestamp",
                    "description": "Timestamp of the sighting",
                    "example": "1648904034"
                  },
                  "attachment": {
                    "type": "string",
                    "format": "base64",
                    "description": "Image attachment of the sighting",
                    "nullable": true
                  },
                  "tiger_id": {
                    "type": "string",
                    "format": "uuid",
                    "description": "ID of the tiger",
                    "example": "3d6ad209-d21b-4bde-aedb-e8f93293514b"
                  }
                }
              }
            }
          }
        },
        "responses": {
          "201": {
            "description": "Returned when sighting is successfully created",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "message": {
                      "type": "string",
                      "description": "Success message",
                      "example": "Tiger sighting successfully created"
                    }
                  }
                }
              }
            }
          },
          "400": {
            "$ref": "#/components/responses/400"
          },
          "404": {
            "$ref": "#/components/responses/404"
          },
          "500": {
            "$ref": "#/components/responses/500"
          }
        }
      },
      "get": {
        "summary": "Get list of a tiger's sightings",
        "parameters": [
          {
            "name": "tiger_id",
            "in": "query",
            "description": "Tiger ID you want to list the sightings of",
            "schema": {
              "type": "string",
              "format": "uuid"
            }
          },
          {
            "name": "sighting_timestamp_start_range",
            "in": "query",
            "description": "Specify the sightings' timestamp start range (inclusive)",
            "schema": {
              "type": "integer"
            }
          },
          {
            "name": "sighting_timestamp_end_range",
            "in": "query",
            "description": "Specify the sightings' timestamp end range (exclusive)",
            "schema": {
              "type": "integer"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Returns list of a tiger's sightings",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "data": {
                      "type": "array",
                      "items": {
                        "type": "object",
                        "properties": {
                          "latitude": {
                            "type": "number",
                            "format": "float",
                            "description": "Coordinate (latitude) of the tiger's sighting",
                            "example": "1.385286074287265"
                          },
                          "longitude": {
                            "type": "number",
                            "format": "float",
                            "description": "Coordinate (longitude) of the tiger's sighting",
                            "example": "1.385286074287265"
                          },
                          "timestamp": {
                            "type": "integer",
                            "format": "unix-timestamp",
                            "description": "When the tiger was seen, specified in UNIX timestamp",
                            "example": "1648904034"
                          },
                          "attachment_id": {
                            "type": "string",
                            "format": "uuid",
                            "description": "Attachment ID to the sighting's image. You can use this Attachment ID to retrieve the image on endpoint /api/tiger_tracker/attachment",
                            "nullable": true
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          },
          "400": {
            "$ref": "#/components/responses/400"
          }
        }
      }
    },
    "/api/tiger_tracker/attachment/{attachment_id}": {
      "get": {
        "summary": "Download uploaded attachment of sightings",
        "parameters": [
          {
            "name": "attachment_id",
            "in": "path",
            "description": "Attachment ID",
            "schema": {
              "type": "string",
              "format": "uuid"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Attachment response",
            "content": {
              "image/jpeg": {
                "schema": {
                  "type": "string",
                  "format": "binary"
                }
              }
            }
          },
          "400": {
            "$ref": "#/components/responses/400"
          }
        }
      }
    }
  },
  "components": {
    "responses": {
      "400": {
        "description": "Returned on client error",
        "content": {
          "application/json": {
            "schema": {
              "oneOf": [
                {
                  "type": "object",
                  "properties": {
                    "message": {
                      "type": "string",
                      "description": "Error message",
                      "example": "Bad request"
                    },
                    "code": {
                      "type": "integer",
                      "description": "Error code",
                      "example": 0
                    }
                  }
                },
                {
                  "type": "object",
                  "properties": {
                    "message": {
                      "type": "string",
                      "description": "Error message",
                      "example": "Birth date cannot be less than 0"
                    },
                    "code": {
                      "type": "integer",
                      "description": "Error code",
                      "example": 1001
                    }
                  }
                },
                {
                  "type": "object",
                  "properties": {
                    "message": {
                      "type": "string",
                      "description": "Error message",
                      "example": "Timestamp cannot be less than 0"
                    },
                    "code": {
                      "type": "integer",
                      "description": "Error code",
                      "example": 1002
                    }
                  }
                },
                {
                  "type": "object",
                  "properties": {
                    "message": {
                      "type": "string",
                      "description": "Error message",
                      "example": "Invalid UUID"
                    },
                    "code": {
                      "type": "integer",
                      "description": "Error code",
                      "example": 1003
                    }
                  }
                },
                {
                  "type": "object",
                  "properties": {
                    "message": {
                      "type": "string",
                      "description": "Error message",
                      "example": "Cannot add sighting within 5km of latest sighting"
                    },
                    "code": {
                      "type": "integer",
                      "description": "Error code",
                      "example": 1004
                    }
                  }
                },
                {
                  "type": "object",
                  "properties": {
                    "message": {
                      "type": "string",
                      "description": "Error message",
                      "example": "Cannot add sighting that happen before its latest sighting"
                    },
                    "code": {
                      "type": "integer",
                      "description": "Error code",
                      "example": 1006
                    }
                  }
                }
              ],
              "discriminator": {
                "propertyName": "code"
              }
            }
          }
        }
      },
      "404": {
        "description": "Returned when resources are not found",
        "content": {
          "application/json": {
            "schema": {
              "oneOf": [
                {
                  "type": "object",
                  "properties": {
                    "message": {
                      "type": "string",
                      "description": "Error message",
                      "example": "Tiger not found"
                    },
                    "code": {
                      "type": "integer",
                      "description": "Error code",
                      "example": 1005
                    }
                  }
                }
              ],
              "discriminator": {
                "propertyName": "code"
              }
            }
          }
        }
      },
      "500": {
        "description": "Returned on server error"
      }
    }
  }
}
