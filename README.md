# Tigerhall Kittens project

## API Documentation

You can access the API documentation [here](api.docs.openapi.json)

You can optionally import the OpenAPI file into Postman

## Setup -- Docker Compose

1. Clone this project
2. Run this command:
   ```shell
   docker compose up -d
   ```
3. Run migration with this command:
   ```shell
   docker exec tigerhall_php php artisan migrate
   ```
4. The server will be run at localhost port 8000

## Setup -- Local installation

1. Clone this project, and do `composer install`
2. Copy .env.example to .env
3. Create and configure two databases for
    - Default Laravel database (e.g. `laravel`)
    - TigerTracker module database (e.g. `tigerhall`)
4. Fill out these environment variables on the .env file you copied earlier:
   ```
   DB_CONNECTION=mysql
   DB_HOST=127.0.0.1 
   DB_PORT=3306 
   DB_DATABASE=laravel 
   DB_USERNAME=root 
   DB_PASSWORD=
   
   TIGER_TRACKER_DATABASE_URL=mysql 
   TIGER_TRACKER_DB_HOST=127.0.0.1 
   TIGER_TRACKER_DB_PORT=3306
   TIGER_TRACKER_DB_DATABASE=tigerhall 
   TIGER_TRACKER_DB_USERNAME=root 
   TIGER_TRACKER_DB_PASSWORD=
   ```
5. Do `php artisan migrate`
6. Run the server with `php artisan serve`

## Linting

This project uses PHPStan for linting. To scan the codebase, run:

```shell
vendor/bin/phpstan analyze .
```

## Tests

This project has three type of tests: unit test, integration test, and system test

> **WARNING**
>
> **Do not run these test suites on production environment.**
>
> **These test suites may change database contents**

### Unit test

This test suite will test domain models to ensure the domain logic stays valid. To run this test, run:

```shell
php artisan test --testsuite=Unit
```

or

```shell
docker exec tigerhall_php php artisan test --testsuite=Unit
```

### Integration test

This test suite will test integration between modules and dependencies (such as databases). To run this test, run:

```shell
php artisan test --testsuite=Integration
```

or

```shell
docker exec tigerhall_php php artisan test --testsuite=Integration
```

### System test

This test suite will test the whole system via its public endpoints. To run this test, run:

```shell
php artisan test --testsuite=System
```

or

```shell
docker exec tigerhall_php php artisan test --testsuite=System
```