<?php

namespace App\Modules\TigerTracker\Core\Application\Service\CreateTigerSighting;

use Illuminate\Http\UploadedFile;

class CreateTigerSightingRequest
{
    private string $tiger_id;
    private float $latitude;
    private float $longitude;
    private int $timestamp;
    private ?UploadedFile $attachment;

    /**
     * @param float $latitude
     * @param float $longitude
     * @param int $timestamp
     * @param UploadedFile|null $attachment
     * @param string $tiger_id
     */
    public function __construct(float $latitude, float $longitude, int $timestamp, ?UploadedFile $attachment,
        string $tiger_id
    )
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->timestamp = $timestamp;
        $this->attachment = $attachment;
        $this->tiger_id = $tiger_id;
    }

    public function getTigerId(): string
    {
        return $this->tiger_id;
    }

    public function getLatitude(): float
    {
        return $this->latitude;
    }

    public function getLongitude(): float
    {
        return $this->longitude;
    }

    public function getTimestamp(): int
    {
        return $this->timestamp;
    }

    public function getAttachment(): ?UploadedFile
    {
        return $this->attachment;
    }
}