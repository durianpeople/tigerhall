<?php

namespace App\Modules\TigerTracker\Core\Application\Service\CreateTigerSighting;

use App\Exceptions\ClientException;
use App\Exceptions\ResourceNotFoundException;
use App\Modules\TigerTracker\Core\Domain\Model\Coordinates;
use App\Modules\TigerTracker\Core\Domain\Model\Tiger\TigerId;
use App\Modules\TigerTracker\Core\Domain\Repository\TigerRepository\TigerRepositoryInterface;
use App\Modules\TigerTracker\Core\Domain\Service\ImageAttachmentService\ImageAttachmentServiceInterface;
use Carbon\Carbon;

class CreateTigerSightingService
{
    private ImageAttachmentServiceInterface $image_attachment_service;
    private TigerRepositoryInterface $tiger_repository;

    /**
     * @param ImageAttachmentServiceInterface $image_attachment_service
     * @param TigerRepositoryInterface $tiger_repository
     */
    public function __construct(
        ImageAttachmentServiceInterface $image_attachment_service,
        TigerRepositoryInterface $tiger_repository
    ) {
        $this->image_attachment_service = $image_attachment_service;
        $this->tiger_repository = $tiger_repository;
    }

    /**
     * @throws ResourceNotFoundException|ClientException
     */
    public function execute(CreateTigerSightingRequest $request): void
    {
        $tiger = $this->tiger_repository->find(new TigerId($request->getTigerId()));
        if (!$tiger) {
            throw new ResourceNotFoundException("Tiger not found", 1005);
        }

        $attachment_id = null;
        if ($request->getAttachment() != null) {
            $attachment_id = $this->image_attachment_service->store($request->getAttachment());
        }

        $tiger->addSighting(
            Carbon::createFromTimestamp($request->getTimestamp()),
            new Coordinates($request->getLatitude(), $request->getLongitude()),
            $attachment_id
        );

        $this->tiger_repository->persist($tiger);
    }
}