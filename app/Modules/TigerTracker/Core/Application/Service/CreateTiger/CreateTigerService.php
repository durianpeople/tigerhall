<?php

namespace App\Modules\TigerTracker\Core\Application\Service\CreateTiger;

use App\Exceptions\ClientException;
use App\Modules\TigerTracker\Core\Domain\Model\Coordinates;
use App\Modules\TigerTracker\Core\Domain\Model\Tiger\Sighting;
use App\Modules\TigerTracker\Core\Domain\Model\Tiger\Tiger;
use App\Modules\TigerTracker\Core\Domain\Repository\TigerRepository\TigerRepositoryInterface;
use Carbon\Carbon;

class CreateTigerService
{
    private TigerRepositoryInterface $tiger_repository;

    /**
     * @param TigerRepositoryInterface $tiger_repository
     */
    public function __construct(TigerRepositoryInterface $tiger_repository)
    {
        $this->tiger_repository = $tiger_repository;
    }

    /**
     * @throws ClientException
     */
    public function execute(CreateTigerRequest $request): void
    {
        $tiger = Tiger::create(
            $request->getName(),
            Carbon::createFromTimestamp($request->getBirthDate()),
            new Sighting(
                Carbon::createFromTimestamp(
                    $request->getLastSeenTimestamp()
                ),
                new Coordinates(
                    $request->getLastSeenLatitude(),
                    $request->getLastSeenLongitude()
                )
            )
        );

        $this->tiger_repository->persist($tiger);
    }
}