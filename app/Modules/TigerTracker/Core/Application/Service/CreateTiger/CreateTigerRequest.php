<?php

namespace App\Modules\TigerTracker\Core\Application\Service\CreateTiger;

class CreateTigerRequest
{
    private string $name;
    private int $birth_date;
    private int $last_seen_timestamp;
    private float $last_seen_latitude;
    private float $last_seen_longitude;

    /**
     * @param string $name
     * @param int $birth_date
     * @param int $last_seen_timestamp
     * @param float $last_seen_latitude
     * @param float $last_seen_longitude
     */
    public function __construct(
        string $name,
        int $birth_date,
        int $last_seen_timestamp,
        float $last_seen_latitude,
        float $last_seen_longitude
    ) {
        $this->name = $name;
        $this->birth_date = $birth_date;
        $this->last_seen_timestamp = $last_seen_timestamp;
        $this->last_seen_latitude = $last_seen_latitude;
        $this->last_seen_longitude = $last_seen_longitude;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getBirthDate(): int
    {
        return $this->birth_date;
    }

    public function getLastSeenTimestamp(): int
    {
        return $this->last_seen_timestamp;
    }

    public function getLastSeenLatitude(): float
    {
        return $this->last_seen_latitude;
    }

    public function getLastSeenLongitude(): float
    {
        return $this->last_seen_longitude;
    }
}