<?php

namespace App\Modules\TigerTracker\Core\Application\Service\ListTigerSightings;

use JsonSerializable;

class ListTigerSightingsResponse implements JsonSerializable
{
    private float $latitude;
    private float $longitude;
    private int $timestamp;
    private ?string $attachment_id;

    /**
     * @param float $latitude
     * @param float $longitude
     * @param int $timestamp
     * @param string|null $attachment_id
     */
    public function __construct(float $latitude, float $longitude, int $timestamp, ?string $attachment_id)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->timestamp = $timestamp;
        $this->attachment_id = $attachment_id;
    }

    /**
     * @return array<string, float|int|string>
     */
    public function jsonSerialize(): array
    {
        return [
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'timestamp' => $this->timestamp,
            'attachment_id' => $this->attachment_id,
        ];
    }
}