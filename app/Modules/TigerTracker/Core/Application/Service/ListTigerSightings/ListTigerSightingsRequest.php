<?php

namespace App\Modules\TigerTracker\Core\Application\Service\ListTigerSightings;

class ListTigerSightingsRequest
{
    private string $tiger_id;
    private int $sighting_timestamp_start_range;
    private int $sighting_timestamp_end_range;

    /**
     * @param string $tiger_id
     * @param int $sighting_timestamp_start_range
     * @param int $sighting_timestamp_end_range
     */
    public function __construct(string $tiger_id, int $sighting_timestamp_start_range, int $sighting_timestamp_end_range)
    {
        $this->tiger_id = $tiger_id;
        $this->sighting_timestamp_start_range = $sighting_timestamp_start_range;
        $this->sighting_timestamp_end_range = $sighting_timestamp_end_range;
    }

    public function getTigerId(): string
    {
        return $this->tiger_id;
    }

    public function getSightingTimestampStartRange(): int
    {
        return $this->sighting_timestamp_start_range;
    }

    public function getSightingTimestampEndRange(): int
    {
        return $this->sighting_timestamp_end_range;
    }
}