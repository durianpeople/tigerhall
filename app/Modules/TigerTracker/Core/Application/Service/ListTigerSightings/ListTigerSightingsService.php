<?php

namespace App\Modules\TigerTracker\Core\Application\Service\ListTigerSightings;

use App\Exceptions\ClientException;
use App\Modules\TigerTracker\Core\Domain\Model\Tiger\Sighting;
use App\Modules\TigerTracker\Core\Domain\Model\Tiger\TigerId;
use App\Modules\TigerTracker\Core\Domain\Repository\TigerRepository\TigerRepositoryInterface;
use Carbon\Carbon;

class ListTigerSightingsService
{
    private TigerRepositoryInterface $tiger_repository;

    /**
     * @param TigerRepositoryInterface $tiger_repository
     */
    public function __construct(TigerRepositoryInterface $tiger_repository)
    {
        $this->tiger_repository = $tiger_repository;
    }

    /**
     * @param ListTigerSightingsRequest $request
     * @return ListTigerSightingsResponse[]
     * @throws ClientException
     */
    public function execute(ListTigerSightingsRequest $request): array
    {
        $sightings = $this->tiger_repository->getSightingsOfTiger(
            new TigerId($request->getTigerId()),
            Carbon::createFromTimestamp($request->getSightingTimestampStartRange()),
            Carbon::createFromTimestamp($request->getSightingTimestampEndRange())
        );

        return collect($sightings)->map(
            function (Sighting $sighting): ListTigerSightingsResponse {
                return new ListTigerSightingsResponse(
                    $sighting->getCoordinates()->getLatitude(),
                    $sighting->getCoordinates()->getLongitude(),
                    $sighting->getTimestamp()->getTimestamp(),
                    $sighting->getAttachmentId() ? $sighting->getAttachmentId()->toString() : null
                );
            }
        )->all();
    }
}