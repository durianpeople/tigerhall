<?php

namespace App\Modules\TigerTracker\Core\Application\Service\DownloadAttachment;

use App\Exceptions\ClientException;
use App\Modules\TigerTracker\Core\Domain\Service\ImageAttachmentService\AttachmentId;
use App\Modules\TigerTracker\Core\Domain\Service\ImageAttachmentService\ImageAttachmentServiceInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;

class DownloadAttachmentService
{
    private ImageAttachmentServiceInterface $image_attachment_service;

    /**
     * @param ImageAttachmentServiceInterface $image_attachment_service
     */
    public function __construct(ImageAttachmentServiceInterface $image_attachment_service)
    {
        $this->image_attachment_service = $image_attachment_service;
    }

    /**
     * @throws ClientException
     */
    public function execute(DownloadAttachmentRequest $request): StreamedResponse
    {
        return $this->image_attachment_service->getStream(new AttachmentId($request->getAttachmentId()));
    }
}