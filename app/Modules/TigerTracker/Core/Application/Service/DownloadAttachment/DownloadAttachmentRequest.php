<?php

namespace App\Modules\TigerTracker\Core\Application\Service\DownloadAttachment;

class DownloadAttachmentRequest
{
    private string $attachment_id;

    /**
     * @param string $attachment_id
     */
    public function __construct(string $attachment_id)
    {
        $this->attachment_id = $attachment_id;
    }

    public function getAttachmentId(): string
    {
        return $this->attachment_id;
    }
}