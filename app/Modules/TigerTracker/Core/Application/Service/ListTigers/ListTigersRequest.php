<?php

namespace App\Modules\TigerTracker\Core\Application\Service\ListTigers;

class ListTigersRequest
{
    private int $last_seen_start_range;
    private int $last_seen_end_range;

    /**
     * @param int $last_seen_start_range
     * @param int $last_seen_end_range
     */
    public function __construct(int $last_seen_start_range, int $last_seen_end_range)
    {
        $this->last_seen_start_range = $last_seen_start_range;
        $this->last_seen_end_range = $last_seen_end_range;
    }

    public function getLastSeenStartRange(): int
    {
        return $this->last_seen_start_range;
    }

    public function getLastSeenEndRange(): int
    {
        return $this->last_seen_end_range;
    }
}