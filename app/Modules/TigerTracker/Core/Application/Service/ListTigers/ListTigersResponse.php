<?php

namespace App\Modules\TigerTracker\Core\Application\Service\ListTigers;

use JsonSerializable;

class ListTigersResponse implements JsonSerializable
{
    private string $id;
    private string $name;
    private int $birth_date;
    private int $last_seen_timestamp;
    private float $last_seen_latitude;
    private float $last_seen_longitude;

    /**
     * @param string $id
     * @param string $name
     * @param int $birth_date
     * @param int $last_seen_timestamp
     * @param float $last_seen_latitude
     * @param float $last_seen_longitude
     */
    public function __construct(
        string $id,
        string $name,
        int $birth_date,
        int $last_seen_timestamp,
        float $last_seen_latitude,
        float $last_seen_longitude
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->birth_date = $birth_date;
        $this->last_seen_timestamp = $last_seen_timestamp;
        $this->last_seen_latitude = $last_seen_latitude;
        $this->last_seen_longitude = $last_seen_longitude;
    }

    /**
     * @return array<string, string|float|int|null>
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'birth_date' => $this->birth_date,
            'last_seen_timestamp' => $this->last_seen_timestamp,
            'last_seen_latitude' => $this->last_seen_latitude,
            'last_seen_longitude' => $this->last_seen_longitude,
        ];
    }
}