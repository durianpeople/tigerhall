<?php

namespace App\Modules\TigerTracker\Core\Application\Service\ListTigers;

use App\Modules\TigerTracker\Core\Domain\Repository\TigerRepository\TigerData;
use App\Modules\TigerTracker\Core\Domain\Repository\TigerRepository\TigerRepositoryInterface;
use Carbon\Carbon;

class ListTigersService
{
    private TigerRepositoryInterface $tiger_repository;

    /**
     * @param TigerRepositoryInterface $tiger_repository
     */
    public function __construct(TigerRepositoryInterface $tiger_repository)
    {
        $this->tiger_repository = $tiger_repository;
    }

    /**
     * @param ListTigersRequest $request
     * @return ListTigersResponse[]
     */
    public function execute(ListTigersRequest $request): array
    {
        $tigers_data = $this->tiger_repository->getTigersList(
            Carbon::createFromTimestamp($request->getLastSeenStartRange()),
            Carbon::createFromTimestamp($request->getLastSeenEndRange())
        );

        return collect($tigers_data)->map(
            function (TigerData $data): ListTigersResponse {
                return new ListTigersResponse(
                    $data->getId()->toString(),
                    $data->getName(),
                    $data->getBirthDate()->getTimestamp(),
                    $data->getLatestSighting()->getTimestamp()->getTimestamp(),
                    $data->getLatestSighting()->getCoordinates()->getLatitude(),
                    $data->getLatestSighting()->getCoordinates()->getLongitude()
                );
            }
        )->all();
    }
}