<?php

namespace App\Modules\TigerTracker\Core\Domain\Repository\TigerRepository;

use App\Modules\TigerTracker\Core\Domain\Model\Tiger\Sighting;
use App\Modules\TigerTracker\Core\Domain\Model\Tiger\TigerId;
use DateTime;

/**
 * This class is a read-only representation of Tiger entity.
 *
 * This is a value object, categorized as a use case optimal query (Vaughn Vernon)
 */
class TigerData
{
    private TigerId $id;
    private string $name;
    private DateTime $birth_date;
    private Sighting $latest_sighting;

    /**
     * @param TigerId $id
     * @param string $name
     * @param Sighting $latest_sighting
     * @param DateTime $birth_date
     */
    public function __construct(TigerId $id, string $name, Sighting $latest_sighting, \DateTime $birth_date)
    {
        $this->id = $id;
        $this->name = $name;
        $this->latest_sighting = $latest_sighting;
        $this->birth_date = $birth_date;
    }

    public function getId(): TigerId
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLatestSighting(): Sighting
    {
        return $this->latest_sighting;
    }

    public function getBirthDate(): DateTime
    {
        return $this->birth_date;
    }
}