<?php

namespace App\Modules\TigerTracker\Core\Domain\Repository\TigerRepository;

use App\Exceptions\ClientException;
use App\Modules\TigerTracker\Core\Domain\Model\Tiger\Sighting;
use App\Modules\TigerTracker\Core\Domain\Model\Tiger\Tiger;
use App\Modules\TigerTracker\Core\Domain\Model\Tiger\TigerId;
use DateTime;

interface TigerRepositoryInterface
{
    /**
     * Find a Tiger by its ID
     *
     * @param TigerId $id
     * @return Tiger|null
     * @throws ClientException
     */
    public function find(TigerId $id): ?Tiger;

    /**
     * Persist changes of a Tiger to repository
     *
     * @param Tiger $tiger
     */
    public function persist(Tiger $tiger): void;

    /**
     * Get Tigers list
     *
     * @param DateTime $last_seen_start_range
     * @param DateTime $last_seen_end_range
     * @return TigerData[]
     */
    public function getTigersList(DateTime $last_seen_start_range, DateTime $last_seen_end_range): array;

    /**
     * Get sightings of a Tiger
     *
     * @param TigerId $id
     * @param DateTime $sighting_timestamp_start_range
     * @param DateTime $sighting_timestamp_end_range
     * @return Sighting[]
     */
    public function getSightingsOfTiger(
        TigerId $id,
        \DateTime $sighting_timestamp_start_range,
        \DateTime $sighting_timestamp_end_range
    ): array;
}