<?php

namespace App\Modules\TigerTracker\Core\Domain\Service\ImageAttachmentService;

use Illuminate\Http\UploadedFile;
use Symfony\Component\HttpFoundation\StreamedResponse;

interface ImageAttachmentServiceInterface
{
    public function store(UploadedFile $file): AttachmentId;

    public function erase(AttachmentId $attachment_id): void;

    public function getStream(AttachmentId $attachment_id): StreamedResponse;
}