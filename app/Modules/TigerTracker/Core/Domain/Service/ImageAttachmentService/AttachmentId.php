<?php

namespace App\Modules\TigerTracker\Core\Domain\Service\ImageAttachmentService;

use App\Modules\TigerTracker\Core\Domain\Model\ConstructableUuidIdTrait;

class AttachmentId
{
    use ConstructableUuidIdTrait;
}