<?php

namespace App\Modules\TigerTracker\Core\Domain\Model;

class Kilometer
{
    private float $value;

    /**
     * @param float $kilometer
     */
    public function __construct(float $kilometer)
    {
        $this->value = $kilometer;
    }

    public function lessThanOrEqualTo(Kilometer $kilometer): bool
    {
        return $this->value <= $kilometer->value;
    }

    public function greaterThan(Kilometer $kilometer): bool
    {
        return $this->value > $kilometer->value;
    }

    public function getValue(): float
    {
        return $this->value;
    }
}