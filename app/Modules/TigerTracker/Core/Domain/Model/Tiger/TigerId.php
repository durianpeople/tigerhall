<?php

namespace App\Modules\TigerTracker\Core\Domain\Model\Tiger;

use App\Modules\TigerTracker\Core\Domain\Model\ConstructableUuidIdTrait;

class TigerId
{
    use ConstructableUuidIdTrait;
}