<?php

namespace App\Modules\TigerTracker\Core\Domain\Model\Tiger;

use App\Exceptions\ClientException;
use App\Modules\TigerTracker\Core\Domain\Model\Coordinates;
use App\Modules\TigerTracker\Core\Domain\Service\ImageAttachmentService\AttachmentId;
use DateTime;

class Sighting
{
    private DateTime $timestamp;
    private Coordinates $coordinates;
    private ?AttachmentId $attachment_id;

    /**
     * @param DateTime $timestamp
     * @param Coordinates $coordinates
     * @param AttachmentId|null $attachment_id
     * @throws ClientException
     */
    public function __construct(DateTime $timestamp, Coordinates $coordinates, ?AttachmentId $attachment_id = null)
    {
        if ($timestamp->getTimestamp() < 0) {
            throw new ClientException("Timestamp cannot be less than 0", 1002);
        }

        $this->timestamp = $timestamp;
        $this->coordinates = $coordinates;
        $this->attachment_id = $attachment_id;
    }

    public function getTimestamp(): DateTime
    {
        return $this->timestamp;
    }

    public function getCoordinates(): Coordinates
    {
        return $this->coordinates;
    }

    public function getAttachmentId(): ?AttachmentId
    {
        return $this->attachment_id;
    }
}