<?php

namespace App\Modules\TigerTracker\Core\Domain\Model\Tiger;

use App\Exceptions\ClientException;
use App\Modules\TigerTracker\Core\Domain\Model\Coordinates;
use App\Modules\TigerTracker\Core\Domain\Model\Kilometer;
use App\Modules\TigerTracker\Core\Domain\Service\ImageAttachmentService\AttachmentId;
use DateTime;
use Ramsey\Uuid\Uuid;

class Tiger
{
    private TigerId $id;
    private string $name;
    private DateTime $birth_date;
    private Sighting $latest_sighting;

    /**
     * This attribute is used only for repository purposes
     *
     * Repository will insert sightings in this attribute into its table when persisting
     *
     * @var Sighting[]
     */
    private array $added_sightings;

    /**
     * @param TigerId $id
     * @param string $name
     * @param DateTime $birth_date
     * @param Sighting $latest_sighting
     * @throws ClientException
     */
    public function __construct(
        TigerId $id,
        string $name,
        DateTime $birth_date,
        Sighting $latest_sighting
    ) {
        if ($birth_date->getTimestamp() < 0) {
            throw new ClientException("Birth date cannot be less than 0", 1001);
        }

        $this->id = $id;
        $this->name = $name;
        $this->birth_date = $birth_date;
        $this->latest_sighting = $latest_sighting;
        $this->added_sightings = [];
    }

    /**
     * Create a new instance of a Tiger
     *
     * @param string $name
     * @param DateTime $birth_date
     * @param Sighting $latest_sighting
     * @return Tiger
     * @throws ClientException
     */
    public static function create(string $name, DateTime $birth_date, Sighting $latest_sighting): Tiger
    {
        $tiger = new Tiger(
            new TigerId(Uuid::uuid4()),
            $name,
            $birth_date,
            $latest_sighting
        );
        $tiger->added_sightings[] = $latest_sighting;
        return $tiger;
    }

    /**
     * Add sighting to a tiger
     *
     * Only sighting outside 5 km of the latest sighting is allowed to be added
     *
     * @param DateTime $timestamp
     * @param Coordinates $coordinates
     * @param AttachmentId|null $attachment_id
     * @return void
     * @throws ClientException 1002, 1004
     */
    public function addSighting(
        DateTime $timestamp,
        Coordinates $coordinates,
        ?AttachmentId $attachment_id = null
    ): void {
        if ($this->latest_sighting
            ->getCoordinates()
            ->distanceTo($coordinates)
            ->lessThanOrEqualTo(new Kilometer(5))) {
            throw new ClientException("Cannot add sighting within 5km of latest sighting", 1004);
        }

        if ($timestamp <= $this->latest_sighting->getTimestamp()) {
            throw new ClientException("Cannot add sighting that happen before its latest sighting", 1006);
        }

        $sighting = new Sighting($timestamp, $coordinates, $attachment_id);
        $this->latest_sighting = $sighting;

        $this->added_sightings[] = $sighting;
    }

    public function getLatestSighting(): Sighting
    {
        return $this->latest_sighting;
    }

    public function getId(): TigerId
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getBirthDate(): DateTime
    {
        return $this->birth_date;
    }

    /**
     * @return Sighting[]
     */
    public function getAddedSightings(): array
    {
        return $this->added_sightings;
    }
}