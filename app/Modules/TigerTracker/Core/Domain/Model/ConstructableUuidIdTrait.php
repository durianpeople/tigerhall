<?php

namespace App\Modules\TigerTracker\Core\Domain\Model;

use App\Exceptions\ClientException;
use Ramsey\Uuid\Uuid;

trait ConstructableUuidIdTrait
{
    private string $guid;

    /**
     * ConstructableUuidId constructor.
     * @param string $uuid
     * @throws ClientException
     */
    public function __construct(string $uuid)
    {
        if (!Uuid::isValid($uuid)) {
            throw new ClientException("Invalid UUID", 1003);
        }
        $this->guid = $uuid;
    }

    public function toString(): string
    {
        return $this->guid;
    }
}