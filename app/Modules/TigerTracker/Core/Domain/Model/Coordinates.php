<?php

namespace App\Modules\TigerTracker\Core\Domain\Model;

class Coordinates
{
    private float $latitude;
    private float $longitude;

    /**
     * @param float $latitude
     * @param float $longitude
     */
    public function __construct(float $latitude, float $longitude)
    {
        $this->latitude = round($latitude, 8);
        $this->longitude = round($longitude, 8);
    }

    public function distanceTo(Coordinates $other_coordinates): Kilometer
    {
        $distance = $this->haversineGreatCircleDistance(
            $this->latitude,
            $this->longitude,
            $other_coordinates->latitude,
            $other_coordinates->longitude
        );

        return new Kilometer($distance);
    }

    /**
     * Measure the distance between two coordinates (given latitude & longitude)
     *
     * The output of this function will be in kilometers.
     *
     * We can extract this function to a separate, utility package, only if necessary
     *
     * @param float $latitudeFrom
     * @param float $longitudeFrom
     * @param float $latitudeTo
     * @param float $longitudeTo
     * @return float
     */
    private function haversineGreatCircleDistance(
        float $latitudeFrom,
        float $longitudeFrom,
        float $latitudeTo,
        float $longitudeTo
    ): float {
        $earthRadius = 6371; // default, units are in kilometer

        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(
                sqrt(
                    pow(sin($latDelta / 2), 2) +
                    cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)
                )
            );
        return $angle * $earthRadius;
    }

    public function getLatitude(): float
    {
        return $this->latitude;
    }

    public function getLongitude(): float
    {
        return $this->longitude;
    }
}