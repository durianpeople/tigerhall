<?php

namespace App\Modules\TigerTracker\Infrastructure\Service;

use App\Exceptions\ClientException;
use App\Modules\TigerTracker\Core\Domain\Service\ImageAttachmentService\AttachmentId;
use App\Modules\TigerTracker\Core\Domain\Service\ImageAttachmentService\ImageAttachmentServiceInterface;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\StreamedResponse;

class IlluminateImageAttachmentService implements ImageAttachmentServiceInterface
{

    /**
     * @throws ClientException
     */
    public function store(UploadedFile $file): AttachmentId
    {
        $attachment_id = new AttachmentId(Uuid::uuid4());
        Storage::put(
            'Tiger/Sightings/' . $attachment_id->toString() . '.jpg',
            Image::make($file)->resize(250, 250)->encode('jpg')->getEncoded(),
        );

        return $attachment_id;
    }

    public function erase(AttachmentId $attachment_id): void
    {
        Storage::delete('Tiger/Sightings/' . $attachment_id->toString() . '.jpg');
    }

    public function getStream(AttachmentId $attachment_id): StreamedResponse
    {
        return Storage::download('Tiger/Sightings/' . $attachment_id->toString() . '.jpg');
    }
}