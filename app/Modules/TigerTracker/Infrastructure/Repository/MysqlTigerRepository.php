<?php

namespace App\Modules\TigerTracker\Infrastructure\Repository;

use App\Exceptions\ClientException;
use App\Modules\TigerTracker\Core\Domain\Model\Coordinates;
use App\Modules\TigerTracker\Core\Domain\Model\Tiger\Sighting;
use App\Modules\TigerTracker\Core\Domain\Model\Tiger\Tiger;
use App\Modules\TigerTracker\Core\Domain\Model\Tiger\TigerId;
use App\Modules\TigerTracker\Core\Domain\Repository\TigerRepository\TigerData;
use App\Modules\TigerTracker\Core\Domain\Repository\TigerRepository\TigerRepositoryInterface;
use App\Modules\TigerTracker\Core\Domain\Service\ImageAttachmentService\AttachmentId;
use Carbon\Carbon;
use DateTime;
use Illuminate\Database\ConnectionInterface;
use Throwable;

class MysqlTigerRepository implements TigerRepositoryInterface
{
    private ConnectionInterface $db;

    /**
     * @param ConnectionInterface $db
     */
    public function __construct(ConnectionInterface $db)
    {
        $this->db = $db;
    }

    /**
     * @throws ClientException
     */
    public function find(TigerId $id): ?Tiger
    {
        $row = $this->db->table('tiger')
            ->where('id', $id->toString())
            ->first();
        if (!$row) {
            return null;
        }

        $latest_sighting = new Sighting(
            Carbon::createFromTimestamp($row->latest_sighting_timestamp),
            new Coordinates($row->latest_sighting_coordinates_latitude, $row->latest_sighting_coordinates_longitude),
            $row->latest_sighting_attachment_id ? new AttachmentId($row->latest_sighting_attachment_id) : null
        );

        return new Tiger(
            $id,
            $row->name,
            Carbon::createFromTimestamp($row->birth_date),
            $latest_sighting
        );
    }

    public function persist(Tiger $tiger): void
    {
        $this->db->beginTransaction();
        try {
            $this->db->table('tiger')->upsert(
                [
                    'id' => $tiger->getId()->toString(),
                    'name' => $tiger->getName(),
                    'birth_date' => $tiger->getBirthDate()->getTimestamp(),
                    'latest_sighting_timestamp' => $tiger->getLatestSighting()->getTimestamp()->getTimestamp(),
                    'latest_sighting_coordinates_latitude' => $tiger
                        ->getLatestSighting()->getCoordinates()->getLatitude(),
                    'latest_sighting_coordinates_longitude' => $tiger
                        ->getLatestSighting()->getCoordinates()->getLongitude(),
                    'latest_sighting_attachment_id' => $tiger->getLatestSighting()->getAttachmentId() ?
                        $tiger->getLatestSighting()->getAttachmentId()->toString() : null,
                ],
                [
                    'id',
                ]
            );

            $this->db->table('tiger_sighting')->insert(
                collect($tiger->getAddedSightings())->map(function (Sighting $sighting) use ($tiger): array {
                    return [
                        'tiger_id' => $tiger->getId()->toString(),
                        'timestamp' => $sighting->getTimestamp()->getTimestamp(),
                        'coordinates_latitude' => $sighting->getCoordinates()->getLatitude(),
                        'coordinates_longitude' => $sighting->getCoordinates()->getLongitude(),
                        'attachment_id' => $sighting->getAttachmentId() ? $sighting->getAttachmentId()->toString(
                        ) : null,
                    ];
                })->all()
            );

            $this->db->commit();
        } catch (Throwable $e) {
            $this->db->rollBack();
            throw $e;
        }
    }

    /**
     * @param DateTime $last_seen_start_range
     * @param DateTime $last_seen_end_range
     * @return TigerData[]
     */
    public function getTigersList(DateTime $last_seen_start_range, DateTime $last_seen_end_range): array
    {
        $tiger_rows = $this->db->select(
            "
                    select id,
                   name,
                   birth_date,
                   latest_sighting_timestamp,
                   latest_sighting_coordinates_latitude,
                   latest_sighting_coordinates_longitude,
                   latest_sighting_attachment_id
            from tiger
                    where latest_sighting_timestamp >= ? and latest_sighting_timestamp < ?
                order by latest_sighting_timestamp desc;",
            [
                $last_seen_start_range->getTimestamp(),
                $last_seen_end_range->getTimestamp(),
            ]
        );

        return collect($tiger_rows)->map(
            function (object $tiger_row): TigerData {
                return new TigerData(
                    new TigerId($tiger_row->id),
                    $tiger_row->name,
                    new Sighting(
                        Carbon::createFromTimestamp($tiger_row->latest_sighting_timestamp),
                        new Coordinates(
                            $tiger_row->latest_sighting_coordinates_latitude,
                            $tiger_row->latest_sighting_coordinates_longitude
                        ),
                        $tiger_row->latest_sighting_attachment_id ? new AttachmentId(
                            $tiger_row->latest_sighting_attachment_id
                        ) : null
                    ),
                    Carbon::createFromTimestamp($tiger_row->birth_date)
                );
            }
        )->all();
    }

    /**
     * @param TigerId $id
     * @param DateTime $sighting_timestamp_start_range
     * @param DateTime $sighting_timestamp_end_range
     * @return Sighting[]
     */
    public function getSightingsOfTiger(
        TigerId $id,
        \DateTime $sighting_timestamp_start_range,
        \DateTime $sighting_timestamp_end_range
    ): array {
        return $this->db->table('tiger_sighting')
            ->where('tiger_id', $id->toString())
            ->where('timestamp', '>=', $sighting_timestamp_start_range->getTimestamp())
            ->where('timestamp', '<', $sighting_timestamp_end_range->getTimestamp())
            ->orderBy('timestamp', 'desc')
            ->get()
            ->map(function (object $row): Sighting {
                return new Sighting(
                    Carbon::createFromTimestamp($row->timestamp),
                    new Coordinates(
                        $row->coordinates_latitude,
                        $row->coordinates_longitude
                    ),
                    $row->attachment_id ? new AttachmentId($row->attachment_id) : null
                );
            })->all();
    }
}