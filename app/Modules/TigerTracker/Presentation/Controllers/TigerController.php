<?php

namespace App\Modules\TigerTracker\Presentation\Controllers;

use App\Exceptions\ClientException;
use App\Exceptions\ResourceNotFoundException;
use App\Http\Controllers\Controller;
use App\Modules\TigerTracker\Core\Application\Service\CreateTiger\CreateTigerRequest;
use App\Modules\TigerTracker\Core\Application\Service\CreateTiger\CreateTigerService;
use App\Modules\TigerTracker\Core\Application\Service\CreateTigerSighting\CreateTigerSightingRequest;
use App\Modules\TigerTracker\Core\Application\Service\CreateTigerSighting\CreateTigerSightingService;
use App\Modules\TigerTracker\Core\Application\Service\DownloadAttachment\DownloadAttachmentRequest;
use App\Modules\TigerTracker\Core\Application\Service\DownloadAttachment\DownloadAttachmentService;
use App\Modules\TigerTracker\Core\Application\Service\ListTigers\ListTigersRequest;
use App\Modules\TigerTracker\Core\Application\Service\ListTigers\ListTigersService;
use App\Modules\TigerTracker\Core\Application\Service\ListTigerSightings\ListTigerSightingsRequest;
use App\Modules\TigerTracker\Core\Application\Service\ListTigerSightings\ListTigerSightingsService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;

class TigerController extends Controller
{
    public function createTiger(Request $request, CreateTigerService $service): JsonResponse
    {
        if ($request->getContentType() != "json") {
            return response()->json(
                [
                    "message" => "Bad request",
                    "code" => 0,
                ]
            );
        }

        $service_request = new CreateTigerRequest(
            $request->json('name'),
            $request->json('birth_date'),
            $request->json('last_seen_timestamp'),
            $request->json('last_seen_latitude'),
            $request->json('last_seen_longitude')
        );

        try {
            $service->execute($service_request);
            return \response()
                ->json(
                    [
                        "message" => "Tiger successfully created",
                    ],
                    201
                )
                ->header('Content-Type', 'text/plain');
        } catch (ClientException $e) {
            return \response()->json(
                [
                    'message' => $e->getMessage(),
                    'code' => $e->getCode(),
                ],
                400
            );
        }
    }

    public function listTigers(Request $request, ListTigersService $service): JsonResponse
    {
        $service_request = new ListTigersRequest(
            (int)$request->query('last_seen_start_range'),
            (int)$request->query('last_seen_end_range')
        );

        $response = $service->execute($service_request);

        return response()->json(
            [
                'data' => $response,
            ]
        );
    }

    public function createTigerSighting(Request $request, CreateTigerSightingService $service): JsonResponse
    {
        $service_request = new CreateTigerSightingRequest(
            (float)$request->post('latitude'),
            (float)$request->post('longitude'),
            (int)$request->post('timestamp'),
            $request->file('attachment'),
            $request->post('tiger_id')
        );

        try {
            $service->execute($service_request);

            return \response()
                ->json(
                    [
                        "message" => "Tiger sighting successfully created",
                    ],
                    201
                )
                ->header('Content-Type', 'text/plain');
        } catch (ClientException $e) {
            return \response()->json(
                [
                    'message' => $e->getMessage(),
                    'code' => $e->getCode(),
                ],
                400
            );
        } catch (ResourceNotFoundException $e) {
            return \response()->json(
                [
                    'message' => $e->getMessage(),
                    'code' => $e->getCode(),
                ],
                404
            );
        }
    }

    public function listTigerSightings(Request $request, ListTigerSightingsService $service): JsonResponse
    {
        $service_request = new ListTigerSightingsRequest(
            $request->query('tiger_id'),
            (int)$request->query('sighting_timestamp_start_range'),
            (int)$request->query('sighting_timestamp_end_range')
        );

        try {
            $response = $service->execute($service_request);
        } catch (ClientException $e) {
            return \response()->json(
                [
                    'message' => $e->getMessage(),
                    'code' => $e->getCode(),
                ],
                400
            );
        }

        return response()->json(
            [
                'data' => $response,
            ]
        );
    }

    /**
     * @param string $attachment_id
     * @param DownloadAttachmentService $service
     * @return JsonResponse|StreamedResponse
     */
    public function downloadAttachment(string $attachment_id, DownloadAttachmentService $service)
    {
        $service_request = new DownloadAttachmentRequest($attachment_id);

        try {
            return $service->execute($service_request);
        } catch (ClientException $e) {
            return \response()->json(
                [
                    'message' => $e->getMessage(),
                    'code' => $e->getCode(),
                ],
                400
            );
        }
    }
}