<?php

use App\Modules\TigerTracker\Presentation\Controllers\TigerController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/tigers', [TigerController::class, 'createTiger']);
Route::get('/tigers', [TigerController::class, 'listTigers']);
Route::post('/tiger/sightings', [TigerController::class, 'createTigerSighting']);
Route::get('/tiger/sightings', [TigerController::class, 'listTigerSightings']);
Route::get('/attachment/{attachment_id}', [TigerController::class, 'downloadAttachment']);
