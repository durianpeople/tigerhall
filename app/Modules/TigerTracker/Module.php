<?php

namespace App\Modules\TigerTracker;

use Dptsi\Modular\Base\BaseModule;

class Module extends BaseModule
{
    public function getProviders(): array
    {
        return [
            \App\Modules\TigerTracker\Providers\RouteServiceProvider::class,
            \App\Modules\TigerTracker\Providers\DatabaseServiceProvider::class,
            \App\Modules\TigerTracker\Providers\ViewServiceProvider::class,
            \App\Modules\TigerTracker\Providers\LangServiceProvider::class,
            \App\Modules\TigerTracker\Providers\BladeComponentServiceProvider::class,
            \App\Modules\TigerTracker\Providers\DependencyServiceProvider::class,
            \App\Modules\TigerTracker\Providers\EventServiceProvider::class,
            \App\Modules\TigerTracker\Providers\MessagingServiceProvider::class,
        ];
    }
}