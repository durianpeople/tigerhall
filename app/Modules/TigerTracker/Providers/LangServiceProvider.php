<?php

namespace App\Modules\TigerTracker\Providers;

use Illuminate\Support\Facades\Lang;
use Illuminate\Support\ServiceProvider;

class LangServiceProvider extends ServiceProvider
{
    protected string $module_name = 'TigerTracker';
    protected string $lang_path = '../Presentation/resources/lang';

    public function boot(): void
    {
        /** @phpstan-ignore-next-line  */
        Lang::addNamespace($this->module_name, __DIR__ . '/' . $this->lang_path);
    }
}