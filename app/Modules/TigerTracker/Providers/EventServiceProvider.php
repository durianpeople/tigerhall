<?php

namespace App\Modules\TigerTracker\Providers;

use Dptsi\Modular\Facade\Module;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /** @phpstan-ignore-next-line */
    protected $listen = [

    ];

    public function shouldDiscoverEvents(): bool
    {
        return true;
    }

    /**
     * @return string[]
     */
    protected function discoverEventsWithin(): array
    {
        return [
            Module::path('TigerTracker', 'Core/Application/EventListeners'),
        ];
    }
}