<?php

namespace App\Modules\TigerTracker\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    protected string $prefix = 'tiger_tracker';
    protected string $module_name = 'tiger_tracker';
    protected string $route_path = '../Presentation/routes';

    public function boot()
    {
        $this->routes(function () {
            Route::prefix('api/' . $this->prefix)
                ->middleware('api')
                ->group(__DIR__ . '/' . $this->route_path . '/api.php');
        });
    }
}