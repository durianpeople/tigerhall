<?php

namespace App\Modules\TigerTracker\Providers;

use App\Modules\TigerTracker\Core\Domain\Repository\TigerRepository\TigerRepositoryInterface;
use App\Modules\TigerTracker\Core\Domain\Service\ImageAttachmentService\ImageAttachmentServiceInterface;
use App\Modules\TigerTracker\Infrastructure\Repository\MysqlTigerRepository;
use App\Modules\TigerTracker\Infrastructure\Service\IlluminateImageAttachmentService;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class DependencyServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(
            TigerRepositoryInterface::class,
            MysqlTigerRepository::class
        );

        $this->app
            ->when([
                       MysqlTigerRepository::class,
                   ])
            ->needs(ConnectionInterface::class)
            ->give(function () {
                return DB::connection('tiger_tracker');
            });

        $this->app->bind(
            ImageAttachmentServiceInterface::class,
            IlluminateImageAttachmentService::class
        );
    }
}