<?php

namespace App\Modules\TigerTracker\Providers;

use Dptsi\Modular\Facade\Messaging;
use Illuminate\Support\ServiceProvider;

class MessagingServiceProvider extends ServiceProvider
{
    protected string $module_name = 'tiger_tracker';

    public function register()
    {
    }

    public function boot(): void
    {
        Messaging::setChannel('tiger_tracker');
//        Messaging::listenTo();
    }
}