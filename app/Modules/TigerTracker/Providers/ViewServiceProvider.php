<?php

namespace App\Modules\TigerTracker\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    protected string $module_name = 'TigerTracker';
    protected string $view_path = '../Presentation/resources/views';

    public function boot(): void
    {
        View::addNamespace($this->module_name, __DIR__ . '/' . $this->view_path);
    }
}