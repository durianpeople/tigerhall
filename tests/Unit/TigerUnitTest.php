<?php

namespace Tests\Unit;

use App\Exceptions\ClientException;
use App\Modules\TigerTracker\Core\Domain\Model\Coordinates;
use App\Modules\TigerTracker\Core\Domain\Model\Tiger\Sighting;
use App\Modules\TigerTracker\Core\Domain\Model\Tiger\Tiger;
use App\Modules\TigerTracker\Core\Domain\Model\Tiger\TigerId;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use Tests\TestCase;

class TigerUnitTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     * @throws ClientException
     */
    public function testCanBeCreated(): void
    {
        $tiger = Tiger::create(
            "The Tiger",
            $birth_date = Carbon::createFromDate(2020, 2, 2),
            $latest_sightings = new Sighting(now(), new Coordinates(1.3432207471832271, 103.85760943678717))
        );

        // Assert the name is still intact
        $this->assertEquals("The Tiger", $tiger->getName());

        // Assert the birth date is still intact
        $this->assertEquals($birth_date, $tiger->getBirthDate());

        // Assert the latest sighting is still intact
        $this->assertEquals($latest_sightings, $tiger->getLatestSighting());
    }

    /**
     * @throws ClientException
     */
    public function testCanAddSightingOutside5Kilometer(): void
    {
        $tiger = new Tiger(
            new TigerId(Uuid::uuid4()),
            "The Tiger",
            $birth_date = Carbon::createFromDate(2020, 2, 2),
            new Sighting(now(), new Coordinates(1.3432207471832271, 103.85760943678717))
        );

        $new_sighting = new Sighting(now(), new Coordinates(1.3171855919169124, 103.89590763790989));
        $tiger->addSighting($new_sighting->getTimestamp(), $new_sighting->getCoordinates(), null);

        // Assert that the new sighting is now the latest sighting of this tiger
        $this->assertEquals($new_sighting, $tiger->getLatestSighting());

        // Assert that there are now 1 added sighting of this tiger (one from its initial creation)
        $this->assertCount(1, $tiger->getAddedSightings());
    }

    /**
     * @throws ClientException
     */
    public function testCannotAddSightingWithin5Kilometer(): void
    {
        $tiger = Tiger::create(
            "The Tiger",
            $birth_date = Carbon::createFromDate(2020, 2, 2),
            $latest_sightings = new Sighting(now(), new Coordinates(1.3432207471832271, 103.85760943678717))
        );

        // Assert that we should not be able to add a sighting within 5 km of its previous sighting
        $this->expectException(ClientException::class);

        $new_sighting = new Sighting(now(), new Coordinates(1.318425022070917, 103.894068007843));
        $tiger->addSighting($new_sighting->getTimestamp(), $new_sighting->getCoordinates(), null);
    }
}
