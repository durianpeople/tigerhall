<?php

namespace Tests\Unit;

use App\Modules\TigerTracker\Core\Domain\Model\Kilometer;
use Tests\TestCase;

class KilometerUnitTest extends TestCase
{
    public function testCanBeCompared(): void
    {
        $this->assertTrue((new Kilometer(5))->lessThanOrEqualTo(new Kilometer(5)));
        $this->assertTrue((new Kilometer(5))->lessThanOrEqualTo(new Kilometer(6)));
        $this->assertFalse((new Kilometer(5))->lessThanOrEqualTo(new Kilometer(4)));

        $this->assertTrue((new Kilometer(5))->greaterThan(new Kilometer(4)));
        $this->assertFalse((new Kilometer(5))->greaterThan(new Kilometer(5)));
        $this->assertFalse((new Kilometer(5))->greaterThan(new Kilometer(6)));
    }
}