<?php

namespace Tests\Unit;

use App\Modules\TigerTracker\Core\Domain\Model\Coordinates;
use App\Modules\TigerTracker\Core\Domain\Model\Kilometer;
use Tests\TestCase;

class CoordinatesUnitTest extends TestCase
{
    public function testCanAccuratelyMeasureDistance(): void
    {
        $origin_coordinates = new Coordinates(1.3432207471832271, 103.85760943678717);
        $coordinates_within_5km = new Coordinates(1.318425022070917, 103.894068007843);
        $coordinates_outside_5km = new Coordinates(1.3171855919169124, 103.89590763790989);

        $this->assertTrue($origin_coordinates->distanceTo($coordinates_within_5km)->lessThanOrEqualTo(new Kilometer(5)));
        $this->assertTrue($origin_coordinates->distanceTo($coordinates_outside_5km)->greaterThan(new Kilometer(5)));
    }
}