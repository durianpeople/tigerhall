<?php

namespace Tests\System;

use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class SystemTest extends TestCase
{
    public function testCreateTigers(): void
    {
        $this->setupCreateTigers();

        $response = $this->postJson('/api/tiger_tracker/tigers', [
            'name' => 'test tiger name',
            'birth_date' => 1649027129,
            'last_seen_timestamp' => 1649027129,
            "last_seen_latitude" => 1.385286074287265,
            "last_seen_longitude" => 103.79614166886488,
        ]);

        $response->assertStatus(201);
    }

    private function setupCreateTigers(): void
    {
        DB::connection('tiger_tracker')->table('tiger_sighting')->delete();
        DB::connection('tiger_tracker')->table('tiger')->delete();
    }

    /**
     * @depends testCreateTigers
     */
    public function testListTigersNonEmpty(): void
    {
        $response = $this->get(
            '/api/tiger_tracker/tigers?last_seen_start_range=0&last_seen_end_range=1649027130'
        );

        $response->assertStatus(200);
        $response->assertJsonCount(1, 'data');
        $response->assertJsonStructure(
            [
                'data' => [
                    '*' => [
                        'id',
                        'name',
                        'birth_date',
                        'last_seen_timestamp',
                        'last_seen_latitude',
                        'last_seen_longitude',
                    ],
                ],
            ]
        );
    }

    /**
     * @depends testCreateTigers
     */
    public function testListTigersEmpty(): void
    {
        $response = $this->get(
            '/api/tiger_tracker/tigers?last_seen_start_range=0&last_seen_end_range=1649027128'
        );

        $response->assertStatus(200);
        $response->assertJsonCount(0, 'data');
    }
}
