<?php

namespace Tests\Integration;

use App\Exceptions\ClientException;
use App\Modules\TigerTracker\Core\Domain\Model\Coordinates;
use App\Modules\TigerTracker\Core\Domain\Model\Tiger\Sighting;
use App\Modules\TigerTracker\Core\Domain\Model\Tiger\Tiger;
use App\Modules\TigerTracker\Core\Domain\Repository\TigerRepository\TigerData;
use App\Modules\TigerTracker\Core\Domain\Repository\TigerRepository\TigerRepositoryInterface;
use App\Modules\TigerTracker\Core\Domain\Service\ImageAttachmentService\AttachmentId;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use Tests\TestCase;

class TigerRepositoryTest extends TestCase
{
    public function testCanPersistAndFind(): void
    {
        $original_tiger = $this->createTiger();

        /** @var TigerRepositoryInterface $repository */
        $repository = resolve(TigerRepositoryInterface::class);
        $repository->persist($original_tiger);

        $retrieved_tiger = $repository->find($original_tiger->getId());

        $this->assertEquals($original_tiger->getId(), $retrieved_tiger->getId());
        $this->assertEquals($original_tiger->getLatestSighting(), $retrieved_tiger->getLatestSighting());
        $this->assertEquals($original_tiger->getBirthDate(), $retrieved_tiger->getBirthDate());
        $this->assertEquals($original_tiger->getName(), $retrieved_tiger->getName());
    }

    /**
     * @return Tiger
     */
    private function createTiger(): Tiger
    {
        $original_tiger = Tiger::create(
            "Merlion",
            Carbon::createFromDate(2020, 2, 2)->startOfDay(),
            new Sighting(
                Carbon::createFromDate(2022, 2, 3)->startOfDay(),
                new Coordinates(1.2867849694936018, 103.85450130045457)
            )
        );

        $original_tiger->addSighting(
            Carbon::createFromDate(2022, 2, 4)->startOfDay(),
            new Coordinates(1.2900955697716912, 103.80390830970096),
            new AttachmentId(Uuid::uuid4())
        );

        $original_tiger->addSighting(
            Carbon::createFromDate(2022, 2, 5)->startOfDay(),
            new Coordinates(1.3502458559671449, 103.80938142764757)
        );
        return $original_tiger;
    }

    /**
     * @throws ClientException
     */
    public function testGetTigersList(): void
    {
        $this->clearTigerDatabase();

        $tigers = $this->createTigers();

        $tigers_data = $this->convertTigersToTigersData($tigers);

        /** @var TigerRepositoryInterface $repository */
        $repository = resolve(TigerRepositoryInterface::class);

        foreach ($tigers as $tiger) {
            $repository->persist($tiger);
        }

        $retrieved_tigers_data = $repository->getTigersList(
            Carbon::createFromDate(2022, 2, 1)->startOfDay(),
            Carbon::createFromDate(2022, 2, 4)->startOfDay()
        );

        $this->assertEquals($tigers_data, $retrieved_tigers_data);
    }

    private function clearTigerDatabase(): void
    {
        DB::connection('tiger_tracker')->table('tiger_sighting')->delete();
        DB::connection('tiger_tracker')->table('tiger')->delete();
        $a = 1 <=> 2;
    }

    /**
     * @return Tiger[]
     * @throws ClientException
     */
    private function createTigers(): array
    {
        return [
            Tiger::create(
                "Tiger A",
                Carbon::createFromDate(2020, 2, 2)->startOfDay(),
                new Sighting(
                    Carbon::createFromDate(2022, 2, 3)->startOfDay(),
                    new Coordinates(1.2867849694936018, 103.85450130045457)
                )
            ),
            Tiger::create(
                "Tiger B",
                Carbon::createFromDate(2020, 2, 2)->startOfDay(),
                new Sighting(
                    Carbon::createFromDate(2022, 2, 2)->startOfDay(),
                    new Coordinates(1.2867849694936018, 103.85450130045457)
                )
            ),
            Tiger::create(
                "Tiger C",
                Carbon::createFromDate(2020, 2, 1)->startOfDay(),
                new Sighting(
                    Carbon::createFromDate(2022, 2, 1)->startOfDay(),
                    new Coordinates(1.2867849694936018, 103.85450130045457)
                )
            ),
        ];
    }

    /**
     * @param Tiger[] $tigers
     * @return TigerData[]
     */
    private function convertTigersToTigersData(array $tigers): array
    {
        return collect($tigers)->map(function (Tiger $tiger): TigerData {
            return new TigerData(
                $tiger->getId(),
                $tiger->getName(),
                $tiger->getLatestSighting(),
                $tiger->getBirthDate()
            );
        })->all();
    }
}