<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyTigerTrackerTigerTableAddLatestSightings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('tiger_tracker')->table('tiger', function (Blueprint $table) {
            $table->integer('latest_sighting_timestamp');
            $table->decimal('latest_sighting_coordinates_latitude', 10, 8);
            $table->decimal('latest_sighting_coordinates_longitude', 11, 8);
            $table->uuid('latest_sighting_attachment_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('tiger_tracker')->table('tiger', function (Blueprint $table) {
            $table->dropColumn('latest_sighting_timestamp');
            $table->dropColumn('latest_sighting_coordinates_latitude');
            $table->dropColumn('latest_sighting_coordinates_longitude');
            $table->dropColumn('latest_sighting_attachment_id');
        });
    }
}
