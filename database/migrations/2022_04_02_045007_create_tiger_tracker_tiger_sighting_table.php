<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTigerTrackerTigerSightingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('tiger_tracker')->create('tiger_sighting', function (Blueprint $table) {
            $table->foreignUuid('tiger_id')->references('id')->on('tiger');
            $table->integer('timestamp')->index();
            $table->decimal('coordinates_latitude', 10, 8);
            $table->decimal('coordinates_longitude', 11, 8);
            $table->uuid('attachment_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('tiger_tracker')->dropIfExists('tiger_sighting');
    }
}
