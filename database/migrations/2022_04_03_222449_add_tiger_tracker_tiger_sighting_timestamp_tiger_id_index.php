<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTigerTrackerTigerSightingTimestampTigerIdIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('tiger_tracker')->table('tiger_sighting', function (Blueprint $table) {
            $table->index(['tiger_id', 'timestamp'], 'tiger_sighting_tiger_id_timestamp_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('tiger_tracker')->table('tiger_sighting', function (Blueprint $table) {
            $table->dropIndex('tiger_sighting_tiger_id_timestamp_index');
        });
    }
}
