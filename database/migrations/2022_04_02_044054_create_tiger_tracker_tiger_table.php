<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTigerTrackerTigerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('tiger_tracker')->create('tiger', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->integer('birth_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('tiger_tracker')->dropIfExists('tiger');
    }
}
